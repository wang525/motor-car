import { NgModule } from '@angular/core';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { LayoutComponent } from './layout.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { OffsidebarComponent } from './offsidebar/offsidebar.component';
import { FooterComponent } from './footer/footer.component';

import { SharedModule } from '../shared/shared.module';
import { RightsidebarComponent } from './rightsidebar/rightsidebar.component';

@NgModule({
    imports: [
        SharedModule,
        NgMultiSelectDropDownModule.forRoot(),
    ],
    providers: [
    ],
    declarations: [
        LayoutComponent,
        SidebarComponent,
        HeaderComponent,
        OffsidebarComponent,
        FooterComponent,
        RightsidebarComponent
    ],
    exports: [
        LayoutComponent,
        SidebarComponent,
        HeaderComponent,
        OffsidebarComponent,
        FooterComponent
    ]
})
export class LayoutModule { }
