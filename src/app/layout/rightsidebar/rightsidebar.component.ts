import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';

@Component({
    selector: 'app-rightsidebar',
    templateUrl: './rightsidebar.component.html',
    styleUrls: ['./rightsidebar.component.scss']
})
export class RightsidebarComponent implements OnInit {

    connectsPeople: Array<any> = [];

    constructor(
        private router: Router
    ) { }

    ngOnInit() {
        this.connectsPeople = [
            {id: 0, img: "assets/img/user/02.jpg", status: "on"},
            {id: 1, img: "assets/img/user/03.jpg", status: "on"},
            {id: 2, img: "assets/img/user/04.jpg", status: "off"},
            {id: 3, img: "assets/img/user/05.jpg", status: "off"},
            {id: 4, img: "assets/img/user/06.jpg", status: "on"},
            {id: 5, img: "assets/img/user/07.jpg", status: "off"},
            {id: 6, img: "assets/img/user/08.jpg", status: "off"},
            {id: 7, img: "assets/img/user/09.jpg", status: "on"},
            {id: 8, img: "assets/img/user/10.jpg", status: "on"},
            {id: 9, img: "assets/img/user/11.jpg", status: "on"}
        ];
    }

    gotoChat(idx) {
        this.router.navigate(['chat']);
    }

}
