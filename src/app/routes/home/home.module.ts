import { NgModule } from '@angular/core';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { NgxSelectModule } from 'ngx-select-ex';
import { SharedModule } from '../../shared/shared.module';
import { LayoutModule } from '../../layout/layout.module';
import { CarCarouselComponent } from '../resources/car-carousel/car-carousel.component';

const routes: Routes = [
    { path: '', component: HomeComponent },
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes),
        NgMultiSelectDropDownModule.forRoot(),
        NgxSelectModule,
        LayoutModule,
    ],
    declarations: [
        HomeComponent,
        CarCarouselComponent,
    ],
    exports: [
        RouterModule,
    ],
})
export class HomeModule { }
