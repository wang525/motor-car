import { Component, OnInit } from "@angular/core";
import { SettingsService } from "../../../core/settings/settings.service";
import { NguCarouselConfig } from "@ngu/carousel";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.scss"],
})
export class HomeComponent implements OnInit {
    // ng2Select
    public items: Array<string> = [
        "Amsterdam",
        "Antwerp",
        "Athens",
        "Barcelona",
        "Berlin",
    ];

    public value: any = {};
    public _disabledV: string = "0";
    public disabled: boolean = false;
    public active = 1;

    // Car Type
    public carType = 0;

    // Charge Type
    public chargeType = 0; // 0 - cash, 1 - finance

    // Main Car Slide
    public carReviewSlides: Array<any> = [];

    // Details
    public carsInfo = [
        { "make": "BMW", "model": "3-series", "mils": "75K-Mils", "price": "$3000", "demoImg": "assets/img/home/review/bg(1).jpg", "description": "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.", "gallery": [] },
        { "make": "BMW", "model": "3-series", "mils": "75K-Mils", "price": "$3000", "demoImg": "assets/img/home/review/bg(2).jpg", "description": "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.", "gallery": [] },
        { "make": "BMW", "model": "3-series", "mils": "75K-Mils", "price": "$3000", "demoImg": "assets/img/home/review/bg(3).jpg", "description": "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.", "gallery": [] },
        { "make": "BMW", "model": "3-series", "mils": "75K-Mils", "price": "$3000", "demoImg": "assets/img/home/review/bg(4).jpg", "description": "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.", "gallery": [] },
        { "make": "BMW", "model": "3-series", "mils": "75K-Mils", "price": "$3000", "demoImg": "assets/img/home/review/bg(5).jpg", "description": "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.", "gallery": [] },
        { "make": "BMW", "model": "3-series", "mils": "75K-Mils", "price": "$3000", "demoImg": "assets/img/home/review/bg(6).jpg", "description": "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.", "gallery": [] },
        { "make": "BMW", "model": "3-series", "mils": "75K-Mils", "price": "$3000", "demoImg": "assets/img/home/review/bg(7).jpg", "description": "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.", "gallery": [] },
        { "make": "BMW", "model": "3-series", "mils": "75K-Mils", "price": "$3000", "demoImg": "assets/img/home/review/bg(8).jpg", "description": "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.", "gallery": [] },
    ];

    // Main Type
    public mainTypeList = [
      { "price": 25000, "carMake": "BMW", "carModel": "3-Series", "mils": "75K-Mils", "carName": "Petrol"},
      { "price": 25023, "carMake": "FORD", "carModel": "5-Series", "mils": "15K-Mils", "carName": "Petrol"},
      { "price": 35200, "carMake": "BMW", "carModel": "3-Series", "mils": "75K-Mils", "carName": "Petrol"},
      { "price": 20550, "carMake": "FORD", "carModel": "5-Series", "mils": "15K-Mils", "carName": "Petrol"},
      { "price": 95060, "carMake": "BMW", "carModel": "3-Series", "mils": "75K-Mils", "carName": "Petrol"},
    ]
    public mainTypeCaroselConfig: NguCarouselConfig = {
      grid: { sm: 1, md: 1, lg: 1, xs: 1, all: 0 },
      slide: 1,
      speed: 300,
      interval: { timing: 3000, initialDelay: 0},
      point: {
        visible: false,
        hideOnSingleSlide: false
      },
      load: 2,
      velocity: 0.2,
      loop: true,
      touch: true,
      easing: 'cubic-bezier(0, 0, 0.2, 1)'
    };

    public carDetailInterval = 3000;

    dropdownList: Array<any> = [];
    selectedItems0: Array<any> = [];
    selectedItems1: Array<any> = [];
    selectedItems2: Array<any> = [];
    selectedItems3: Array<any> = [];
    selectedItems4: Array<any> = [];
    selectedItems5: Array<any> = [];

    dropdownSettings: any = {};

    constructor(public settings: SettingsService) {
        this.settings.setLayoutSetting("sidebarHidden", true);

        // init carousel
        for (let i = 1; i <= 20; i++) {
            this.addReviewSlide(i);
        }
    }

    ngOnInit() {
        this.dropdownList = [
            { item_id: 1, item_text: 'BMW' },
            { item_id: 2, item_text: 'Toyoda' },
            { item_id: 3, item_text: 'Hindo' },
            { item_id: 4, item_text: 'Merc' },
            { item_id: 5, item_text: '4Wagon' },
            { item_id: 6, item_text: 'Safari' },
            { item_id: 7, item_text: 'Yok' },
            { item_id: 8, item_text: 'Jipsi' },
            { item_id: 9, item_text: 'Dongche' },
            { item_id: 10, item_text: 'Train' },
            { item_id: 11, item_text: 'Ship' },
            { item_id: 12, item_text: 'Goat' },
            { item_id: 13, item_text: 'Tank' }
        ];

        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };
    }

    ngOnDestroy() {
        this.settings.setLayoutSetting("sidebarHidden", false);
    }

    onItemSelect(item: any) {
        console.log(item);
    }
    onSelectAll(items: any) {
        console.log(items);
    }

    public get disabledV(): string {
        return this._disabledV;
    }

    public set disabledV(value: string) {
        this._disabledV = value;
        this.disabled = this._disabledV === "1";
    }

    public selected(value: any): void {
        console.log("Selected value is: ", value);
    }

    public removed(value: any): void {
        console.log("Removed value is: ", value);
    }

    public typed(value: any): void {
        console.log("New search input: ", value);
    }

    public refreshValue(value: any): void {
        this.value = value;
    }

    public addReviewSlide(id = 20): void {
        this.carReviewSlides.push({
            "image": "assets/img/home/review/bg(" + id + ").jpg",
            "carType": "2012 Mercedes-Benz CLS 320",
            "mils": 25000 + id * 10,
            "engine": 2.5 + id / 10,
            "price": 25000 + id * 100,
        });
    }

    public selectCarType(id) {
        this.carType = id;
    }

    public selectCashType() {
        this.chargeType = 0;
    }

    public selectFinanceType() {
        this.chargeType = 1;
    }

}
