
const Home = {
    text: 'Home',
    link: '/home',
    icon: 'icon-home'
};

const Category = {
    text: 'Category',
    link: '/cars',
    icon: 'icon-home'
}

const NonCategory = {
    text: 'Non-Category',
    link: '/noncategory',
    icon: 'icon-home'
}

const AutoDealer = {
    text: 'AutoDealer',
    link: '/autodealer',
    icon: 'icon-home'
}

export const menu = [
    Category,
    NonCategory,
    AutoDealer
];
