import { LayoutComponent } from '../layout/layout.component';
import { Error404Component } from './pages/error404/error404.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { RecoverComponent } from './pages/recover/recover.component';

export const routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: './home/home.module#HomeModule' },
    { path: 'register', component: RegisterComponent },
    { path: 'login', component: LoginComponent },
    { path: 'recover', component: RecoverComponent },
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', loadChildren: './resources/resources.module#ResourcesModule' },
        ]
    },

    { path: '404', component: Error404Component },

    // Not found
    { path: '**', redirectTo: '404' }

];
