import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AutodealerComponent } from './autodealer.component';

describe('AutodealerComponent', () => {
  let component: AutodealerComponent;
  let fixture: ComponentFixture<AutodealerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AutodealerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AutodealerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
