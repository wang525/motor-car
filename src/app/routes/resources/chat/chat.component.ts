import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';

@Component({
    selector: 'app-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewChecked  {

    @ViewChild('scrollMessageHistory') private msgHistoryContainer: ElementRef;

    connectsPeople: Array<any> = [];
    selectedPerson: any = null;
    msgs: Array<any> = [];
    curMsg: any = null;

    constructor() { }

    ngOnInit() {
        this.connectsPeople = [
            {id: 0, img: "assets/img/user/02.jpg", status: "on", name: "John Doe", lastDate: "Dec 25", lastMsg: "This is the last msg"},
            {id: 1, img: "assets/img/user/03.jpg", status: "on", name: "Jackson Jin", lastDate: "Dec 25", lastMsg: "This is the last msg"},
            {id: 2, img: "assets/img/user/04.jpg", status: "off", name: "Mike Doe", lastDate: "Dec 25", lastMsg: "This is the last msg"},
            {id: 3, img: "assets/img/user/05.jpg", status: "off", name: "Alexa Bliss", lastDate: "Dec 21", lastMsg: "This is the last msg"},
            {id: 4, img: "assets/img/user/06.jpg", status: "on", name: "Roman Reigns", lastDate: "Dec 5", lastMsg: "This is the last msg"},
            {id: 5, img: "assets/img/user/07.jpg", status: "off", name: "Brock Lesnar", lastDate: "Dec 2", lastMsg: "This is the last msg"},
            {id: 6, img: "assets/img/user/08.jpg", status: "off", name: "Kevin Owins", lastDate: "Dec 22", lastMsg: "This is the last msg"},
            {id: 7, img: "assets/img/user/09.jpg", status: "on", name: "Michael Joe", lastDate: "Dec 21", lastMsg: "This is the last msg"},
            {id: 8, img: "assets/img/user/10.jpg", status: "on", name: "Kane Dil", lastDate: "Dec 25", lastMsg: "This is the last msg"},
            {id: 9, img: "assets/img/user/11.jpg", status: "on", name: "Sunil Kumar", lastDate: "Dec 25", lastMsg: "This is the last msg"}
        ];

        if (this.connectsPeople && this.connectsPeople.length > 0) {
            this.selectedPerson = this.connectsPeople[0];
        }

        this.msgs = [
            {received: {msg: "This is the test", date: "11:01 AM | June 9"}, send: {msg: "Are you okay? Please reply to me ahaha", date: "11:01 AM | June 10"}},
            {received: {msg: "This is the test", date: "11:01 AM | June 9"}, send: {msg: "Are you okay? Please reply to me ahaha", date: "11:01 AM | June 10"}},
            {received: {msg: "This is the test", date: "11:01 AM | June 9"}, send: {msg: "Are you okay? Please reply to me ahaha", date: "11:01 AM | June 10"}},
            {received: {msg: "This is the test", date: "11:01 AM | June 9"}, send: {msg: "Are you okay? Please reply to me ahaha", date: "11:01 AM | June 10"}},
            {received: {msg: "This is the test", date: "11:01 AM | June 9"}, send: {msg: "Are you okay? Please reply to me ahaha", date: "11:01 AM | June 10"}},
            {received: {msg: "This is the test", date: "11:01 AM | June 9"}, send: {msg: "Are you okay? Please reply to me ahaha", date: "11:01 AM | June 10"}},
            {received: {msg: "This is the test", date: "11:01 AM | June 9"}, send: {msg: "Are you okay? Please reply to me ahaha", date: "11:01 AM | June 10"}},
        ];

        this.scrollToBottom();
    }

    selectPerson(id) {
        this.selectedPerson = this.connectsPeople[id];
    }

    sendMsg() {

        if (this.curMsg) {
            let msg = {
                send: {msg: this.curMsg, date: "12:20 PM | May 20"}
            };
            this.msgs.push(msg);
            this.curMsg = "";
        }
    }

    scrollToBottom(): void {
      try {
          this.msgHistoryContainer.nativeElement.scrollTop = this.msgHistoryContainer.nativeElement.scrollHeight;
      } catch(err) { }
    }

    ngAfterViewChecked() {
      this.scrollToBottom();
    }

}
