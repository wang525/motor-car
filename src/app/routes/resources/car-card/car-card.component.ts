import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-car-card',
    templateUrl: './car-card.component.html',
    styleUrls: ['./car-card.component.scss']
})
export class CarCardComponent implements OnInit {

    @Input() car: any = null;

    @Output() public showOverview: EventEmitter<any> = new EventEmitter();

    constructor(
        public router: Router
    ) { }

    ngOnInit() {
    }

    clickShowOverview($event) {
        $event.preventDefault();
        this.showOverview.emit();
    }

    gotoDetail($event) {
        $event.preventDefault();
        this.router.navigate(['/cars/aaa']);
    }
}
