import { Component, OnInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { Router } from '@angular/router';
import { NguCarouselConfig } from '@ngu/carousel';

@Component({
    selector: 'app-car-overview',
    templateUrl: './car-overview.component.html',
    styleUrls: ['./car-overview.component.scss']
})
export class CarOverviewComponent implements OnInit, AfterViewChecked {

    show = false;

    public imgLists = [];
    public mainImage = "assets/img/home/main-card.png";

    public carouselTile: NguCarouselConfig = {
        grid: { sm: 2, md: 4, lg: 4, xs: 4, all: 0 },
        slide: 1,
        speed: 300,
        interval: { timing: 2000, initialDelay: 0 },
        point: {
            visible: false,
            hideOnSingleSlide: false
        },
        load: 2,
        velocity: 0.2,
        loop: false,
        touch: true,
        easing: 'cubic-bezier(0, 0, 0.2, 1)'
    };

    constructor(
        public settings: SettingsService,
        public router: Router,
        private cdRef: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.imgLists = [
            { id: 0, image: "assets/img/home/review/bg(1).jpg" },
            { id: 1, image: "assets/img/home/review/bg(2).jpg" },
            { id: 2, image: "assets/img/home/review/bg(3).jpg" },
            { id: 3, image: "assets/img/home/review/bg(4).jpg" },
            { id: 4, image: "assets/img/home/review/bg(5).jpg" },
            { id: 5, image: "assets/img/home/review/bg(6).jpg" },
            { id: 6, image: "assets/img/home/review/bg(7).jpg" },
            { id: 7, image: "assets/img/home/review/bg(8).jpg" },
            { id: 8, image: "assets/img/home/review/bg(9).jpg" },
        ];
        this.mainImage = this.imgLists[0].image;
    }

    ngAfterViewChecked() {
        let show = this.isShowExpand();
        if (show != this.show) { // check if it change, tell CD update view
            this.show = show;
            this.cdRef.detectChanges();
        }
    }

    public isShowExpand() {

        return true;
    }

    onClose() {
        this.settings.closeOffsidebar();
    }

    gotoDetail($event) {
        $event.preventDefault();
        this.router.navigate(['/cars/aaa']);
    }

    selectImage(id: number) {
        this.mainImage = this.imgLists[id].image;
    }
}
