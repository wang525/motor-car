import { Component, OnInit, ViewChild, Injector, HostListener } from "@angular/core";
import { SettingsService } from "../../../core/settings/settings.service";
import { MenuService } from "../../../core/menu/menu.service";
import { Router } from "@angular/router";

declare var $: any;

@Component({
    selector: "app-cars",
    templateUrl: "./cars.component.html",
    styleUrls: ["./cars.component.scss"],
})
export class CarsComponent implements OnInit {

    // Tablet Width
    tabletWidth = 768;

    //
    cars: Array<any> = [];

    menuItems: Array<any>;
    sbclickEvent = "click.sidebar-toggle";
    $doc: any = null;
    router: Router;

    dropdownList: Array<any> = [];
    selectedItems: Array<any> = [];
    dropdownSettings: any = {};

    public isShownLeftSide = true;
    public innerWidth: any;

    constructor(
        public menu: MenuService,
        public settings: SettingsService,
        public injector: Injector
    ) {
        this.menuItems = menu.getMenu();
    }

    ngOnInit() {
        this.cars = [
            {
                make: "Land Rover Lad Tree",
                model: "Velar",
                mils: "75K-Mils",
                type: "Auto",
                free: true,
                price: "$3000",
                demoImg: "assets/img/home/review/bg(1).jpg",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.",
                gallery: [],
            },
            {
                make: "Land Rover Lad Tree",
                model: "Velar",
                mils: "75K-Mils",
                type: "Auto",
                free: false,
                price: "$3000",
                demoImg: "assets/img/home/review/bg(2).jpg",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.",
                gallery: [],
            },
            {
                make: "Land Rover Lad Tree",
                model: "Velar",
                mils: "75K-Mils",
                type: "Auto",
                free: false,
                price: "$3000",
                demoImg: "assets/img/home/review/bg(3).jpg",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.",
                gallery: [],
            },
            {
                make: "Land Rover Lad Tree",
                model: "Velar",
                mils: "75K-Mils",
                type: "Auto",
                free: true,
                price: "$3000",
                demoImg: "assets/img/home/review/bg(4).jpg",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.",
                gallery: [],
            },
            {
                make: "Land Rover Lad Tree",
                model: "Velar",
                mils: "75K-Mils",
                type: "Auto",
                free: false,
                price: "$3000",
                demoImg: "assets/img/home/review/bg(5).jpg",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.",
                gallery: [],
            },
            {
                make: "Land Rover Lad Tree",
                model: "Velar",
                mils: "75K-Mils",
                type: "Auto",
                free: false,
                price: "$3000",
                demoImg: "assets/img/home/review/bg(6).jpg",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.",
                gallery: [],
            },
            {
                make: "Land Rover Lad Tree",
                model: "Velar",
                mils: "75K-Mils",
                type: "Auto",
                free: false,
                price: "$3000",
                demoImg: "assets/img/home/review/bg(7).jpg",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.",
                gallery: [],
            },
            {
                make: "Land Rover Lad Tree",
                model: "Velar",
                mils: "75K-Mils",
                type: "Auto",
                free: false,
                price: "$3000",
                demoImg: "assets/img/home/review/bg(8).jpg",
                description:
                    "Lorem ipsum dolor sit amet, consectetur adipicsing elit... Aliquam hendrerit sagittis urna.",
                gallery: [],
            },
        ];

        this.dropdownList = [
            { item_id: 1, item_text: "BMW" },
            { item_id: 2, item_text: "Toyoda" },
            { item_id: 3, item_text: "Hindo" },
            { item_id: 4, item_text: "Merc" },
            { item_id: 5, item_text: "4Wagon" },
            { item_id: 6, item_text: "Safari" },
            { item_id: 7, item_text: "Yok" },
            { item_id: 8, item_text: "Jipsi" },
            { item_id: 9, item_text: "Dongche" },
            { item_id: 10, item_text: "Train" },
            { item_id: 11, item_text: "Ship" },
            { item_id: 12, item_text: "Goat" },
            { item_id: 13, item_text: "Tank" },
        ];

        this.dropdownSettings = {
            singleSelection: false,
            idField: "item_id",
            textField: "item_text",
            selectAllText: "Select All",
            unSelectAllText: "UnSelect All",
            itemsShowLimit: 3,
            allowSearchFilter: true,
        };

        this.innerWidth = window.innerWidth;
    }

    ngOnDestroy() {
        if (this.$doc) this.$doc.off(this.sbclickEvent);
    }

    @HostListener('window:resize', ['$event'])
    onResize(event) {

        // Smaller
        if (this.innerWidth > this.tabletWidth && window.innerWidth <= this.tabletWidth) {
            this.isShownLeftSide = this.settings.getLeftsidebar();
        }

        // Larger
        if (this.innerWidth <= this.tabletWidth && window.innerWidth > this.tabletWidth) {
            this.isShownLeftSide = !this.settings.getLeftsidebar();
        }

        console.log(this.settings.getLeftsidebar());

        this.innerWidth = window.innerWidth;
    }

    onItemSelect(item: any) {
        console.log(item);
    }

    onSelectAll(items: any) {
        console.log(items);
    }

    toggleSubmenuClick(event) {
        event.preventDefault();

        if (
            !this.isSidebarCollapsed() &&
            !this.isSidebarCollapsedText() &&
            !this.isEnabledHover()
        ) {
            let target = $(event.target || event.srcElement || event.currentTarget);
            let ul,
                anchor = target;

            // find the UL
            if (!target.is("a")) {
                anchor = target.parent("a").first();
            }
            ul = anchor.next();

            // hide other submenus
            let parentNav = ul.parents(".sidebar-subnav");
            $(".sidebar-subnav").each((idx, el) => {
                let $el = $(el);
                // if element is not a parent or self ul
                if (!$el.is(parentNav) && !$el.is(ul)) {
                    this.closeMenu($el);
                }
            });

            // abort if not UL to process
            if (!ul.length) {
                return;
            }

            // any child menu should start closed
            ul.find(".sidebar-subnav").each((idx, el) => {
                this.closeMenu($(el));
            });

            // toggle UL height
            if (parseInt(ul.height(), 0)) {
                this.closeMenu(ul);
            } else {
                // expand menu
                ul.on("transitionend", () => {
                    ul.height("auto").off("transitionend");
                }).height(ul[0].scrollHeight);
                // add class to manage animation
                ul.addClass("opening");
            }
        }
    }

    // Close menu collapsing height
    closeMenu(elem) {
        elem.height(elem[0].scrollHeight); // set height
        elem.height(0); // and move to zero to collapse
        elem.removeClass("opening");
    }

    toggleSubmenuHover(event) {
        let self = this;
        if (
            this.isSidebarCollapsed() ||
            this.isSidebarCollapsedText() ||
            this.isEnabledHover()
        ) {
            event.preventDefault();

            this.removeFloatingNav();

            let target = $(event.target || event.srcElement || event.currentTarget);
            let ul,
                anchor = target;
            // find the UL
            if (!target.is("a")) {
                anchor = target.parent("a");
            }
            ul = anchor.next();

            if (!ul.length) {
                return; // if not submenu return
            }

            let $aside = $(".aside-container");
            let $asideInner = $aside.children(".aside-inner"); // for top offset calculation
            let $sidebar = $asideInner.children(".sidebar");
            let mar =
                parseInt($asideInner.css("padding-top"), 0) +
                parseInt($aside.css("padding-top"), 0);
            let itemTop = anchor.parent().position().top + mar - $sidebar.scrollTop();

            let floatingNav = ul.clone().appendTo($aside);
            let vwHeight = $(window).height();

            // let itemTop = anchor.position().top || anchor.offset().top;

            floatingNav
                .removeClass("opening") // necesary for demo if switched between normal//collapsed mode
                .addClass("nav-floating")
                .css({
                    position: this.settings.getLayoutSetting("isFixed")
                        ? "fixed"
                        : "absolute",
                    top: itemTop,
                    bottom:
                        floatingNav.outerHeight(true) + itemTop > vwHeight ? 0 : "auto",
                });

            floatingNav
                .on("mouseleave", () => {
                    floatingNav.remove();
                })
                .find("a")
                .on("click", function (e) {
                    e.preventDefault(); // prevents page reload on click
                    // get the exact route path to navigate
                    let routeTo = $(this).attr("route");
                    if (routeTo) self.router.navigate([routeTo]);
                });

            this.listenForExternalClicks();
        }
    }

    listenForExternalClicks() {
        let $doc = $(document).on("click.sidebar", (e) => {
            if (!$(e.target).parents(".aside-container").length) {
                this.removeFloatingNav();
                $doc.off("click.sidebar");
            }
        });
    }

    removeFloatingNav() {
        $(".nav-floating").remove();
    }

    isSidebarCollapsed() {
        return this.settings.getLayoutSetting("isCollapsed");
    }
    isSidebarCollapsedText() {
        return this.settings.getLayoutSetting("isCollapsedText");
    }
    isEnabledHover() {
        return this.settings.getLayoutSetting("asideHover");
    }

    openOffSidebar() {
        this.settings.openOffsidebar();
    }

    showOverview($event) {
        this.openOffSidebar();
    }

    getSelecteVal($event) {
        console.log("selected val", $event.value);
    }

    showLeftSide() {
        if (this.innerWidth > this.tabletWidth) {
            this.settings.setLeftsidebar(false);
        } else {
            console.log("show");
            this.settings.setLeftsidebar(true);
        }
        this.isShownLeftSide = true;
    }

    hiddenLeftSide() {
        if (this.innerWidth > this.tabletWidth) {
            this.settings.setLeftsidebar(true);
        } else {
            this.settings.setLeftsidebar(false);
        }
        this.isShownLeftSide = false;
    }
}
