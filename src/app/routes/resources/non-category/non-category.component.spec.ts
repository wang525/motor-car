import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonCategoryComponent } from './non-category.component';

describe('NonCategoryComponent', () => {
  let component: NonCategoryComponent;
  let fixture: ComponentFixture<NonCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
