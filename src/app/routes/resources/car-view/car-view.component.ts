import { Component, OnInit, AfterViewChecked, ChangeDetectorRef, AfterViewInit, AfterContentChecked } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { NguCarouselConfig } from '@ngu/carousel';

@Component({
    selector: 'app-car-view',
    templateUrl: './car-view.component.html',
    styleUrls: ['./car-view.component.scss']
})
export class CarViewComponent implements OnInit, AfterViewChecked, AfterContentChecked {

    show = false;

    public imgLists = [
        { id: 0, image: "assets/img/home/review/bg(1).jpg" },
        { id: 1, image: "assets/img/home/review/bg(2).jpg" },
        { id: 2, image: "assets/img/home/review/bg(3).jpg" },
        { id: 3, image: "assets/img/home/review/bg(4).jpg" },
        { id: 4, image: "assets/img/home/review/bg(5).jpg" },
        { id: 5, image: "assets/img/home/review/bg(6).jpg" },
        { id: 6, image: "assets/img/home/review/bg(7).jpg" },
        { id: 7, image: "assets/img/home/review/bg(8).jpg" },
        { id: 8, image: "assets/img/home/review/bg(9).jpg" },
    ];

    public mainImage = "assets/img/home/main-card.png";

    public carouselTile: NguCarouselConfig = {
        grid: { sm: 2, md: 4, lg: 4, xs: 4, all: 0 },
        slide: 2,
        speed: 300,
        interval: { timing: 2000, initialDelay: 0 },
        point: {
            visible: false,
            hideOnSingleSlide: false
        },
        load: 2,
        velocity: 0.2,
        loop: false,
        touch: true,
        easing: 'cubic-bezier(0, 0, 0.2, 1)'
    };

    public preventImgItem = true;

    constructor(
        public settings: SettingsService,
        private cdRef: ChangeDetectorRef
    ) { }

    ngOnInit() {

        this.imgLists = [
            { id: 0, image: "assets/img/home/review/bg(1).jpg" },
            { id: 1, image: "assets/img/home/review/bg(2).jpg" },
            { id: 2, image: "assets/img/home/review/bg(3).jpg" },
            { id: 3, image: "assets/img/home/review/bg(4).jpg" },
            { id: 4, image: "assets/img/home/review/bg(5).jpg" },
            { id: 5, image: "assets/img/home/review/bg(6).jpg" },
            { id: 6, image: "assets/img/home/review/bg(7).jpg" },
            { id: 7, image: "assets/img/home/review/bg(8).jpg" },
            { id: 8, image: "assets/img/home/review/bg(9).jpg" },
        ];

        this.mainImage = this.imgLists[0].image;
    }

    ngAfterContentChecked() {
        this.settings.setLayoutSetting("sidebarHidden", true);
    }

    ngAfterViewChecked() {
        let show = this.isShowExpand();
        if (show != this.show) { // check if it change, tell CD update view
            this.show = show;
            this.cdRef.detectChanges();
        }
    }

    public isShowExpand() {

        return true;
    }

    ngOnDestroy() {
        this.settings.setLayoutSetting("sidebarHidden", false);
    }

    selectImage(id: number) {
        this.mainImage = this.imgLists[id].image;
    }

    dragImgItem($event) {
        this.preventImgItem = false;
        console.log("drag");
    }

    mouseupTmgItem() {
        this.preventImgItem = true;
        console.log("mouseup");
    }
}
