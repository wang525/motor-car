import { Component, OnInit } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { SettingsService } from '../../../core/settings/settings.service';

const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-car-post',
    templateUrl: './car-post.component.html',
    styleUrls: ['./car-post.component.scss']
})
export class CarPostComponent implements OnInit {

    public active = 1;
    public selectedId = 0;
    public stepNumber = 3;

    public uploader: FileUploader = new FileUploader({ url: URL });
    public hasBaseDropZoneOver: boolean = false;

    constructor(
        public settings: SettingsService
    ) { }

    ngOnInit() {
        this.settings.setLayoutSetting("sidebarHidden", true);
        this.settings.setLayoutSetting("rightSidebarHidden", true);
    }

    ngOnDestroy() {
        this.settings.setLayoutSetting("sidebarHidden", false);
        this.settings.setLayoutSetting("rightSidebarHidden", false);
    }

    public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public gotoNextStep() {
        this.selectedId += 1;
        if (this.selectedId > this.stepNumber) {
            this.selectedId = this.stepNumber;
        }
    }

    public gotoPreviousStep() {
        this.selectedId -= 1;
        if (this.selectedId < 0) {
            this.selectedId = 0;
        }
    }

    public gotoSubmit() {

    }

}
