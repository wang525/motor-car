import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { CommonModule } from '@angular/common';
import { CarCardComponent } from './car-card/car-card.component';
import { CarOverviewComponent } from './car-overview/car-overview.component';
import { CarViewComponent } from './car-view/car-view.component';
import { CarsComponent } from './cars/cars.component';
import { CarPostComponent } from './car-post/car-post.component';
import { SharedModule } from '../../shared/shared.module';
import { FileUploadModule } from 'ng2-file-upload';
import { TagInputModule } from 'ngx-chips';
import { ChatComponent } from './chat/chat.component';
import { LayoutModule } from '../../layout/layout.module';
import { NonCategoryComponent } from './non-category/non-category.component';
import { AutodealerComponent } from './autodealer/autodealer.component';


const routes: Routes = [
    { path: 'cars', component: CarsComponent},
    { path: 'cars/new', component: CarPostComponent},
    { path: 'cars/:id', component: CarViewComponent},
    { path: 'chat', component: ChatComponent},
    { path: 'noncategory', component: NonCategoryComponent},
    { path: 'autodealer', component: AutodealerComponent},

    // Not found
    { path: '**', redirectTo: '404' }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
        NgMultiSelectDropDownModule.forRoot(),
        CommonModule,
        SharedModule,
        FileUploadModule,
        TagInputModule,
        FormsModule,
        LayoutModule
    ],
    declarations: [
        CarCardComponent,
        CarOverviewComponent,
        CarViewComponent,
        CarsComponent,
        CarPostComponent,
        ChatComponent,
        NonCategoryComponent,
        AutodealerComponent,
    ]
})
export class ResourcesModule { }
