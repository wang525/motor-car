import { Component, OnInit, Input, AfterViewInit, ViewChild, ChangeDetectorRef, AfterViewChecked } from "@angular/core";
import { NguCarouselConfig, NguCarousel } from '@ngu/carousel';
import { interval, Subscription } from 'rxjs';

@Component({
    selector: "app-car-carousel",
    templateUrl: "./car-carousel.component.html",
    styleUrls: ["./car-carousel.component.scss"],
})
export class CarCarouselComponent implements OnInit, AfterViewChecked {
    @Input() data;
    @Input() height;

    // Car Carousal
    public carReviewInterval = 5000;
    public noWrapSlides: boolean = false;
    public carReviewSlides = [];
    public imgHeight = 0;
    public isLooped = true;

    show = false;

    @ViewChild('imgCarousel') imgCarousel: NguCarousel<any>;

    public carouselTile: NguCarouselConfig = {
        grid: { sm: 1, md: 1, lg: 1, xs: 1, all: 0 },
        slide: 1,
        speed: 300,
        interval: { timing: 2000, initialDelay: 0 },
        point: {
            visible: false,
            hideOnSingleSlide: false
        },
        load: 2,
        velocity: 0.2,
        loop: false,
        touch: true,
        easing: 'cubic-bezier(0, 0, 0.2, 1)'
    };

    public imgTimer;

    constructor(
        private cdRef: ChangeDetectorRef
    ) {
    }

    ngOnInit() {
        this.startImageLoop();
    }

    ngAfterViewChecked() {
        let show = this.isShowExpand();
        if (show != this.show) { // check if it change, tell CD update view
            this.show = show;
            this.cdRef.detectChanges();
        }
    }

    ngOnDestroy() {
        if (this.imgTimer) {
            clearInterval(this.imgTimer);
        }
    }

    public isShowExpand() {
        this.carReviewSlides = this.data;
        this.imgHeight = this.height;

        return true;
    }

    public startImageLoop() {
        if (!this.imgTimer) {
            this.slideStart();
        }
    }

    public nextImage() {
        let imgLength = this.carReviewSlides.length;
        let nextId = this.imgCarousel.activePoint + 1;
        if (nextId >= imgLength) {
            nextId = 0;
        }

        this.imgCarousel.moveTo(nextId);
    }

    public slideStop() {
        if (this.imgTimer) {
            clearInterval(this.imgTimer);
        }
    }

    public slideStart() {
        this.imgTimer = setInterval(() => {
            this.nextImage();
        }, this.carReviewInterval);
    }
}
