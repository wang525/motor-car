import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { LayoutModule } from '../../layout/layout.module';

import { Error404Component } from './error404/error404.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { RecoverComponent } from './recover/recover.component';

@NgModule({
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        LayoutModule,
    ],
    declarations: [
        Error404Component,
        RegisterComponent,
        LoginComponent,
        RecoverComponent,
    ],
    exports: [
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        Error404Component,
        RegisterComponent,
        LoginComponent,
        RecoverComponent,
    ]
})
export class PagesModule { }
